<?php
$koneksi = mysqli_connect("localhost", "root", "") or die(mysqli_error($koneksi));
mysqli_select_db($koneksi, "db_umm") or die(mysqli_error($koneksi));
include("../mpdf.php");
$id = $_GET['id'];
$data = mysqli_query($koneksi, "SELECT *, DATE_FORMAT(tgl_surat, '%d') as tanggal, DATE_FORMAT(tgl_surat, '%c')-1 as bulan, DATE_FORMAT(tgl_surat, '%Y') as tahun, DATE_FORMAT(tgl_surat, '%d-%c-%Y') as tgl FROM surat_keluar WHERE id='$id'") or die(mysqli_error());
$data = mysqli_fetch_assoc($data);
$data = (object)$data;
$mpdf=new mPDF('','',12,'Times New Roman',22,22,47,16,9,9); 
$mpdf->SetImportUse();  

$mpdf->SetDocTemplate('SampleSUrat.pdf',1); // 1|0 to continue after end of document or not - used on matching page numbers
$bulan = ['Januari','Februari','','Maret','April','Mei','Juni','','Juli','','Agustus','September','Oktober','November','','Desember'];
$html="<body style='font-family: Times New Roman; font-size: 12pt;'>
<table width='100%'>
  <tr>
    <td width='20%'>&nbsp;</td>
    <td width='5%'>&nbsp;</td>
    <td style='text-align:right'>Malang, $data->tanggal ".$bulan[$data->bulan]." $data->tahun</td>
  </tr>
  <tr>
    <td width='20%'>Nomor</td>
    <td width='5%'>:</td>
    <td>$data->nomor</td>
  </tr>
  <tr>
    <td width='20%'>Lampiran</td>
    <td width='5%'>:</td>
    <td>$data->lampiran</td>
  </tr>
  <tr>
    <td width='20%'>Perihal</td>
    <td width='5%'>:</td>
    <td><b>$data->perihal</b></td>
  </tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>";

  foreach (explode(",", $data->kepada) as $k => $t) {
    $kepada = "";
    $tanda = "";
    if($k == 0){
      $kepada = "Kepada Yth";
      $tanda = ":";
    }
    $html .= "<tr>
    <td width='18%'>$kepada</td>
    <td width='2%'>$tanda</td>
    <td>".trim($t)."</td>
  </tr>";
  }
  $html.="<tr>
    <td width='18%'></td>
    <td width='2%'></td>
    <td>$data->lokasi_tujuan</td>
  </tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>
  <tr>
    <td width='18%'></td>
    <td width='2%'></td>
    <td><b>Assalaamu'alaikum Wr.Wb</b></td>
  </tr>
  <tr>
    <td colspan='3'>";
    for ($i=0; $i < 27; $i++)$html.="&nbsp;";
    $html.= html_entity_decode($data->isi)."</td>
  </tr>
</table>
<table width='100%'>
  <tr>
    <td width='18%'></td>
    <td width='2%'></td>
    <td> <b>Waalaikumsalam</b></td>
  </tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>
</table>";

$html.="<table width=100%>
   <tr>
    <td width=70% colspan=2>&nbsp;</td>
    <td width=30%>$data->atas_nama</td>
  </tr>
   <tr>
    <td width=70% colspan=2>&nbsp;</td>
    <td width=30%>$data->dari_jabatan</td>
  </tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>
  <tr><td> &nbsp; </td></tr>
  <tr>
    <td width=70% colspan=2>&nbsp;</td>
    <td width=30%>$data->dari_nama</td>
  </tr>
  <tr>
    <td width='20%'><b><u>Tembusan Yth</u></b></td>
    <td width='2%' colspan=2>:</td>
  </tr>";
  foreach (explode(",", $data->tembusan) as $k => $t) 
    $html .= "<tr><td colspan=3>".($k+1).'. '.trim($t)."</td></td>";
$html .= "</table>";
//  echo $html;
$mpdf->AddPage();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

?>