<style type="text/css">
  .center{
    text-align: center;
    margin-left: auto;
    margin-right: auto; 
  }
</style>
<?php
$koneksi = mysqli_connect("localhost", "root", "") or die(mysqli_error($koneksi));
mysqli_select_db($koneksi, "db_umm") or die(mysqli_error($koneksi));
$dari = $_POST['dari'];
$sampai = $_POST['sampai'];
function getSuratMasuk(){
  global $dari, $sampai, $koneksi;
  $sql = mysqli_query($koneksi, "SELECT * FROM surat_masuk s LEFT JOIN disposisi d ON s.id = d.id_sMasuk WHERE tgl_surat >= '$dari' AND tgl_surat <= '$sampai' ") or die(mysqli_error());

    while ($r = mysqli_fetch_assoc($sql))
        $data[] = $r;
  return $data;
}

function getSuratKeluar(){
  global $dari, $sampai, $koneksi;
  $sql = mysqli_query($koneksi, "SELECT * FROM surat_keluar WHERE tgl_surat >= '$dari' AND tgl_surat <= '$sampai' ") or die(mysqli_error());

    while ($r = mysqli_fetch_assoc($sql))
        $data[] = $r;
  return $data;
}
include("../mpdf.php");

$mpdf=new mPDF('','',12,'Calibri (Body)',22,22,15,16,9,9); 
// $mpdf->SetImportUse();  

// $mpdf->SetDocTemplate('SampleSUrat.pdf',1); // 1|0 to continue after end of document or not - used on matching page numbers
$dKeluar = "
<h1>LAPORAN SURAT KELUAR</h1>
<p class='pmhMiddleRight'>TANGGAL: $dari sampai $sampai</p>
<p>&nbsp;</p>
<table border=1 width=80%>
<tbody>
  <tr>
    <td>NOMOR SURAT</td>
    <td>PERIHAL</td>
    <td>DARI</td>
    <td>KEPADA</td>
    <td>TGL SURAT</td>
  </tr>";
foreach (getSuratMasuk() as $key => $m) {
  $m = (object)$m;
$dKeluar .= "<tr>
  <td>$m->nomor</td>
  <td>$m->perihal</td>
  <td>$m->dari</td>
  <td>$m->kepada</td>
  <td>$m->tgl_surat</td>
</tr>";
}
$dKeluar .= "</table>";

$mpdf->AddPage();
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);  
$mpdf->WriteHTML($dKeluar);
$mpdf->Output();
exit;

?>