<?php 
$config = [
  'user'=> 
  [
    [
      'field' => 'username',
      'label' => 'Username',
      'rules' => 'required'
    ],
    [
      'field' => 'password',
      'label' => 'Password',
      'rules' => 'required'
    ],
    [
      'field' => 'jabatan',
      'label' => 'Jabatan',
      'rules' => 'required'
    ],
    [
      'field' => 'bagian',
      'label' => 'Bagian',
      'rules' => 'required'
    ]

  ],
  'jenis_surat' => 
  [
    [
      'field' => 'jenis_surat',
      'label' => 'Jenis Surat',
      'rules' => 'required|max_length[25]',
    ]
  ],
  'sMasuk' => 
  [
    [
      'field' => 'Nomor',
      'label' => 'Nomor',
      'rules' => 'required'
    ],
    [
      'field' => 'Dari',
      'label' => 'Dari',
      'rules' => 'required'
    ],
    [
      'field' => 'Lampiran',
      'label' => 'Lampiran',
      'rules' => 'required|integer'
    ],
    [
      'field' => 'Perihal',
      'label' => 'Perihal',
      'rules' => 'required'
    ],
    [
      'field' => 'Kepada',
      'label' => 'Kepada',
      'rules' => 'required'
    ],
    [
      'field' => 'Tgl_Surat',
      'label' => 'Tanggl Surat',
      'rules' => 'required'
    ]
  ],
  'sKeluar' => [
    [
      'field' => 'atas_nama',
      'label' => 'Atas Nama',
      'rules' => 'required'
    ],
    [
      'field' => 'id_jenis',
      'label' => 'Jenis Surat',
      'rules' => 'required'
    ],
    [
      'field' => 'Nomor',
      'label' => 'Nomor Surat',
      'rules' => 'required'
    ],
    [
      'field' => 'lokasi_tujuan',
      'label' => 'Lokasi Tujuan Surat',
      'rules' => 'required'
    ],
    [
      'field' => 'Perihal',
      'label' => 'Perihal',
      'rules' => 'required'
    ],
    [
      'field' => 'Kepada',
      'label' => 'Kepada',
      'rules' => 'required'
    ],
    [
      'field' => 'Tembusan',
      'label' => 'Tembusan',
      'rules' => 'required'
    ],
    [
      'field' => 'Isi',
      'label' => 'Isi',
      'rules' => 'required|max_length[1000]'
    ],
    [
      'field' => 'Dari_nama',
      'label' => 'Nama',
      'rules' => 'required'
    ],
    [
      'field' => 'Dari_jabatan',
      'label' => 'Jabatan',
      'rules' => 'required'
    ],
    [
      'field' => 'Tgl_surat',
      'label' => 'Tanggal Surat',
      'rules' => 'required'
    ],

  ]
];



$config['error_prefix'] = '<label class="error">';
$config['error_suffix'] = '</label>';