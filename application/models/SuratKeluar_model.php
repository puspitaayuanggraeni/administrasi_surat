<?php 
class SuratKeluar_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function find($filter=[]){
    $this->db->where($filter);    
    $query = $this->db->get('surat_keluar');
    return $query->result();
  }

  public function findWhere($filter=[]){
    $this->db->select('s.*, d.jenis_surat');
    $this->db->where($filter);    
    $this->db->from('surat_keluar s');
    $this->db->join('jenis_surat d', 's.id_jenis = d.id', 'INNER');
    $query = $this->db->get();
    return $query->result();
  }



  public function insert($data){
    $this->db->insert('surat_keluar', $data);
    return $this->db->insert_id();
  }

  public function update($id, $data){
    $this->db->where('id', $id);
    $this->db->update('surat_keluar', $data);
  }


  public function delete($id){
    $this->db->where('id', $id);
    $this->db->delete('surat_keluar');
  }
}