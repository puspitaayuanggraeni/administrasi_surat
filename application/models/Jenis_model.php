<?php 
class Jenis_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function find($filter=[]){
    $this->db->where($filter);    
    $query = $this->db->get('jenis_surat');
    return $query->result();
  }

  public function insert($data){
    $this->db->insert('jenis_surat', $data);
  }

  public function update($id, $data){
    $this->db->where('id', $id);
    $this->db->update('jenis_surat', $data);
  }

  public function delete($id){
    echo "$id";
    $this->db->where('id', $id);
    $this->db->delete('jenis_surat');
  }
}