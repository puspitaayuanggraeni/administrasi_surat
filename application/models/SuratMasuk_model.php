<?php 
class SuratMasuk_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function find($filter=[]){
    // $this->db->where($filter);    
    // $query = $this->db->get('surat_masuk');
    $this->db->select('*');
    $this->db->where($filter);    
    $this->db->from('surat_masuk s');
    $this->db->join('disposisi d', 's.id = d.id_sMasuk', 'left');
    $query = $this->db->get();
    return $query->result();
  }

  public function findCount($filter=[]){
    $this->db->where($filter);
    $query = $this->db->get('surat_masuk');
    return $query->num_rows();
  }
  
  public function findScan($filter=[]){
    $this->db->where($filter);
    $query = $this->db->get('scan');
    return $query->result();
  }

  public function insert($data){
    $this->db->insert('surat_masuk', $data);
    return $this->db->insert_id();
  }

  public function insertScan($data){
    $this->db->insert('scan', $data);
    return $this->db->insert_id();
  }

  public function insertDisposisi($data){
    $this->db->insert('disposisi', $data);
    return $this->db->insert_id();
  }

  public function update($id, $data){
    $this->db->where('id', $id);
    $this->db->update('surat_masuk', $data);
  }
  
  public function updateDisposisi($id, $data){
    $this->db->where('id_sMasuk', $id);
    $this->db->update('disposisi', $data);
  }

  public function delete($id){
    $this->db->where('id', $id);
    $this->db->delete('surat_masuk');
  }

  public function deleteScan($filter=[]){
    $this->db->where($filter);
    $this->db->delete('scan');
  }
}