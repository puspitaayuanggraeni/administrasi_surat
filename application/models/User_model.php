<?php 
class User_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function find($filter=[]){
  $this->db->where($filter);    
  $query = $this->db->get('user');
  return $query->result();
  }

  public function insert($data){
    $this->db->insert('user', $data);
  }

  public function update($id, $data){
    $this->db->where('id_user', $id);
    $this->db->update('user', $data);
  }

  public function delete($id){
    $this->db->where('id_user', $id);
    $this->db->delete('user');
  }
}