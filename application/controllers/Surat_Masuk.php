<?php

class Surat_Masuk extends CI_Controller {

  public function __construct() {
    parent::__construct();
    define("BASE_PATH", getcwd());
    $this->load->model('SuratMasuk_model', 'sMasuk');
    if (!hakAkses($this, 'sMasuk'))
      redirect(base_url() . '403');

    $this->data['model'] = $this->sMasuk->find();
    $this->data['values'] = [];

    $config['allowed_types'] = 'jpeg|jpg|png';
    $config['upload_path'] = './uploads/';
    $fileName = date('YYYYmmdd') . time();
    $config['file_name'] = $fileName;
    $this->load->library('upload', $config);
    $this->data['error'] = [];
    $this->data['isNewRecord'] = TRUE;
  }

  public function index() {
    $data = $this->data;
    $data['title'] = 'Surat Masuk';
    $this->load->library('upload');

    load_view($this, 'surat_masuk', $data);
  }

  public function insert() {
    $data = $this->data;
    $data['title'] = 'Surat Masuk';
    $files = [];
    $files['type'] = [];
    $files['name'] = [];
    $files = $_FILES['Isi'];
    $error = [];

    if (strlen($files['name'][0]) > 0) {
      foreach ($files['type'] as $key => $value) {
        if (!strpos("$value", "images"))
          $error['type'] = 'Type not allowed';
      }
    }else {
      $error['required'] = 'You did not select a file to upload.';
    }

    $data['error'] = $error;

    if ($this->form_validation->run('sMasuk') == 'TRUE') {
      $insert = [
        'Nomor' => $this->input->post('Nomor'),
        'Dari' => $this->input->post('Dari'),
        'Lampiran' => $this->input->post('Lampiran'),
        'Perihal' => $this->input->post('Perihal'),
        'Kepada' => $this->input->post('Kepada'),
        'Tgl_Surat' => $this->input->post('Tgl_Surat')
      ];
      $id = $this->sMasuk->insert($insert);

      $images = array();
      foreach ($files['name'] as $key => $image) {
        $_FILES['Isi']['name'] = $files['name'][$key];
        $_FILES['Isi']['type'] = $files['type'][$key];
        $_FILES['Isi']['tmp_name'] = $files['tmp_name'][$key];
        $_FILES['Isi']['error'] = $files['error'][$key];
        $_FILES['Isi']['size'] = $files['size'][$key];

        if ($this->upload->do_upload('Isi')) {
          $insertScan = [
            'id_SMasuk' => $id,
            'image' => $this->upload->data()['file_name']
          ];
          $this->sMasuk->insertScan($insertScan);
        }
      }
      if (trim($this->input->post('Kepada2')) != '') {
        $insertDisposisi = [
          'id_sMasuk' => $id,
          'disposisi_kepada' => $this->input->post('Kepada2'),
          'tgl_disposisi' => $this->input->post('tgl_disposisi'),
          'keterangan' => $this->input->post('Keterangan')
        ];
        $this->sMasuk->insertDisposisi($insertDisposisi);
      }
      redirect(base_url() . 'surat/masuk');
    } else {
      load_view($this, 'surat_masuk', $data);
    }
  }

  public function edit() {
    $data = $this->data;
    $id = $_GET['id'];
    $data['title'] = 'Edit Surat Masuk';
    $data['values'] = (array) $this->sMasuk->find(['s.id' => $id])[0];
    $data['scan'] = $this->sMasuk->findScan(['id_SMasuk' => $id]);
    $data['isNewRecord'] = FALSE;

    load_view($this, 'editSM', $data);
  }

  public function updateAll() {
    $data = $this->data;
    $id = $this->input->get('id');
    $data['title'] = 'Edit Surat Masuk';
    $data['values'] = [];
    $data['scan'] = $this->sMasuk->findScan(['id_SMasuk' => $id]);
    $data['isNewRecord'] = FALSE;
    $files = [];
    $files['type'] = [];
    $files['name'] = [];
    $files = $_FILES['Isi'];
    $error = [];
    if (isset($files['name']) && strlen($files['name'][0]) > 0) {
      foreach ($files['type'] as $key => $value) {
        if (!strpos("$value", "images"))
          $error['type'] = 'Type not allowed';
      }
    }
    $data['error'] = $error;

    if ($this->form_validation->run('sMasuk') == 'TRUE') {
      $insert = [
        'Nomor' => $this->input->post('Nomor'),
        'Dari' => $this->input->post('Dari'),
        'Lampiran' => $this->input->post('Lampiran'),
        'Perihal' => $this->input->post('Perihal'),
        'Kepada' => $this->input->post('Kepada'),
        'Tgl_Surat' => $this->input->post('Tgl_Surat')
      ];
      $this->sMasuk->update($id, $insert);

      $images = array();
      if (strlen($files['name'][0]) > 0) {
        foreach ($files['name'] as $key => $image) {
          $_FILES['Isi']['name'] = $files['name'][$key];
          $_FILES['Isi']['type'] = $files['type'][$key];
          $_FILES['Isi']['tmp_name'] = $files['tmp_name'][$key];
          $_FILES['Isi']['error'] = $files['error'][$key];
          $_FILES['Isi']['size'] = $files['size'][$key];

          if ($this->upload->do_upload('Isi')) {
            $insertScan = [
              'id_SMasuk' => $id,
              'image' => $this->upload->data()['file_name']
            ];
            $this->sMasuk->insertScan($insertScan);
          }
        }
      }
      if (trim($this->input->post('Kepada2')) != '') {
        $insertDisposisi = [
          'id_sMasuk' => $id,
          'disposisi_kepada' => $this->input->post('Kepada2'),
          'tgl_disposisi' => $this->input->post('tgl_disposisi'),
          'keterangan' => $this->input->post('Keterangan')
        ];
        $this->sMasuk->updateDisposisi($id, $insertDisposisi);
      }
      redirect(base_url() . 'surat/masuk/');
    } else {
      load_view($this, 'editSM', $data);
    }
  }

  public function update() {
    $field = $this->input->post('field');
    $data = [
      "$field" => $this->input->post('value')
    ];
    $this->sMasuk->update($this->input->post('id'), $data);
    echo 'success';
  }

  public function delete() {
    $d = $this->sMasuk->find(['id' => $this->input->get('id')]);
    $file = BASE_PATH . '/uploads/' . $d[0]->isi;
    if (file_exists($file))
      unlink($file);

    $this->sMasuk->delete($this->input->get('id'));
    redirect(base_url() . 'surat/masuk');
  }

  public function deleteScan() {
    $file = BASE_PATH . '/uploads/' . $this->input->get('image');
    if (file_exists($file))
      unlink($file);

    $this->sMasuk->deleteScan(['image' => $this->input->get('image'), 'id_SMasuk' => $this->input->get('id')]);
    redirect(base_url() . 'surat/masuk/edit?id=' . $this->input->get('id'));
  }

}
