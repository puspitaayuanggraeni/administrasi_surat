<?php
class Surat_keluar extends CI_Controller {

  public function __construct() {
    parent::__construct();
    define("BASE_PATH", getcwd());
    $this->load->model('SuratKeluar_model', 'sKeluar');
    $this->load->model('jenis_model', 'jenis');
    if(!hakAkses($this, 'sKeluar'))
      redirect(base_url().'403');
    
    $this->data['jenis_surat'] = $this->jenis->find();
    $this->data['values'] = [];
  }

  public function index(){
    $data = $this->data;
    $data['title']='Surat Keluar';
    $data['model'] = $this->sKeluar->findWhere();
    $data['jenis_surat'] = $this->jenis->find();
    
    load_view($this, 'surat_keluar', $data);
  }

  public function edit(){
    $id = $_GET['id'];
    $data = $this->data;
    $data['title']='Edit Surat Keluar';
    $data['values'] = (array)$this->sKeluar->findWhere(['s.id'=>$id])[0];

    load_view($this, 'editSK', $data);
  }

  public function insert(){
    $data = $this->data;
    $data['title']='Surat Keluar';
    $data['model'] = $this->sKeluar->findWhere();

    if($this->form_validation->run('sKeluar') == 'TRUE') {
      $noSurat = $this->input->post('kode_surat').'/'.$this->input->post('Nomor').$this->input->post('Nomor2');
    $insert = [
      'nomor' => $noSurat ,
      'lampiran' => $this->input->post('Lampiran'),
      'perihal' => $this->input->post('Perihal'),
      'id_jenis' => $this->input->post('id_jenis'),
      'kepada' => $this->input->post('Kepada'),
      'tembusan' => $this->input->post('Tembusan'),
      'isi' => $this->input->post('Isi'),
      'dari_nama' => $this->input->post('Dari_nama'),
      'dari_jabatan' => $this->input->post('Dari_jabatan'),
      'tgl_surat' => $this->input->post('Tgl_surat'),
      'atas_nama' => $this->input->post('atas_nama'),
      'lokasi_tujuan' => $this->input->post('lokasi_tujuan')
    ];
    $id = $this->sKeluar->insert($insert);
    redirect(base_url().'surat/keluar');
        
    } else {
        load_view($this, 'surat_keluar', $data);      
    }

  }

  public function update(){
    $field =  $this->input->post('field');
    $data = [
      "$field" => $this->input->post('value')
    ];
    $this->sKeluar->update($this->input->post('id'), $data);
    echo 'successas';
  }  

  public function updateAll(){
    $id = $_GET['id'];
    $data = $this->data;
    $data['title']='Edit Surat Keluar';
    $values['id'] = $id;
    if($this->form_validation->run('sKeluar') == 'TRUE') {        
      $insert = [
        'nomor' => $this->input->post('Nomor'),
        'lampiran' => $this->input->post('Lampiran'),
        'perihal' => $this->input->post('Perihal'),
        'id_jenis' => $this->input->post('id_jenis'),
        'kepada' => $this->input->post('Kepada'),
        'tembusan' => $this->input->post('Tembusan'),
        'isi' => $this->input->post('Isi'),
        'dari_nama' => $this->input->post('Dari_nama'),
        'dari_jabatan' => $this->input->post('Dari_jabatan'),
        'tgl_surat' => $this->input->post('Tgl_surat'),
        'atas_nama' => $this->input->post('atas_nama'),
        'lokasi_tujuan' => $this->input->post('lokasi_tujuan')
      ];
      $this->sKeluar->update($id, $insert);
      redirect(base_url().'surat/keluar');
    } else {
        load_view($this,'editSK', $data);
    }
  }

  public function delete(){
    $this->sKeluar->delete($this->input->get('id'));
    redirect(base_url().'surat/keluar');
  }

}