<?php
class User extends CI_Controller {

  public function __construct() {
    parent::__construct();
    define("BASE_PATH", getcwd());
    $this->load->model('SuratMasuk_model', 'sMasuk');    
    $this->load->model('SuratKeluar_model', 'sKeluar');
  }

  public function login(){
    $this->load->view('login');
  }
  public function loginproses() {
    $_POST['password'] = md5($_POST['password']);
      $available = $this->user->find([
        'username'=>"$_POST[username]",
        'password'=>"$_POST[password]"
      ]);

      if(!$available){
        $data['result'] = 'failed';
        redirect(base_url());
      } else {
        $this->session->set_userdata('id_user', $available[0]->id_user);
        redirect(base_url().'user/dasbor');
      }
  }

  public function index(){
    if(!hakAkses($this, 'user'))
      redirect(base_url().'403');

    $data['title']='User';
    $data['users'] = $this->user->find();

    load_view($this, 'user', $data);
  }

  public function dasbor(){
    if(!hakAkses($this, 'dasbor'))
      redirect(base_url().'403');

    $data['title']='Dasbor';
    $data['users'] = $this->user->find();
    $now= date('Y-m-d');
    $mNow= date('m');
    $yNow= date('Y');
    $data['sMasuk'] = [
      'hari' => $this->sMasuk->findCount(['tgl_surat = $date']),
      'bulan' => $this->sMasuk->findCount(['month(tgl_surat) = $mNow','year(tgl_surat) =$yNow']),
      'tahun' => $this->sMasuk->findCount(['year(tgl_surat) = $yNow'])
    ];
    $data['sKeluar'] = [
      'hari' => count($this->sKeluar->find(['tgl_surat = $date'])),
      'bulan' => count($this->sKeluar->find(['month(tgl_surat) = $mNow','year(tgl_surat) =$yNow'])),
      'tahun' => count($this->sKeluar->find(['year(tgl_surat) = $yNow']))
    ];
    load_view($this, 'dasbor', $data);
  }

  public function logout(){
    session_destroy();
    redirect(base_url());
  }

  public function insert(){
    $data['title']='User';
    $data['users'] = $this->user->find();
    
    if($this->form_validation->run('user') == 'TRUE') {
      $insert = [
        'username' => $this->input->post('username'),
        'password'=> md5($this->input->post('password')),
        'jabatan'=> $this->input->post('jabatan'),
        'bagian' => $this->input->post('bagian')
      ];
      $this->user->insert($insert);
      redirect(base_url().'user/index');
    } else {
      load_view($this, 'user', $data);
    }

  }

  public function update(){
    $field =  $this->input->post('field');
    $data = [
      "$field" => $this->input->post('value')
    ];
    $this->user->update($this->input->post('id'), $data);
    echo 'success';
  }

  public function delete(){
    $this->user->delete($this->input->get('id'));
    redirect(base_url().'user/index');
  }

  public function forbidden(){
    $data['title'] = 'Akses Dilarang !';
    load_view($this, '403', $data);
  }
}