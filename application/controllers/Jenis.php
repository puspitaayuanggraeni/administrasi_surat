<?php
class Jenis extends CI_Controller {

  public function __construct() {
    parent::__construct();
    define("BASE_PATH", getcwd());
    $this->load->model('jenis_model', 'jenis');
    if(!hakAkses($this, 'jSurat'))
      redirect(base_url().'403');
  }

  public function index(){
    $data['title']='Jenis Surat';
    $data['jenis'] = $this->jenis->find();

    load_view($this, 'jenis', $data);
  }

  public function insert(){
    $data['title']='Jenis Surat';
    $data['jenis'] = $this->jenis->find();
    
    if($this->form_validation->run('jenis_surat') == 'TRUE') {
      $insert = [
        'kode_surat' => $this->input->post('kode_surat'),
        'jenis_surat' => $this->input->post('jenis_surat')
      ];
      $this->jenis->insert($insert);
      redirect(base_url().'jenis/index');
    } else {
      load_view($this, 'jenis', $data);      
    }

  }

  public function update(){
    $field =  $this->input->post('field');
    $data = [
      "$field" => $this->input->post('value')
    ];
    $this->jenis->update($this->input->post('id'), $data);
    echo 'success';
  }

  public function delete(){
    $this->jenis->delete($this->input->get('id'));
    redirect(base_url().'jenis/index');
  }

}