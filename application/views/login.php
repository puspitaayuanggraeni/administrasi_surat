<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/ico" href="<?= base_url()?>/public/images/favicon.ico" />


  <title><?= title_app;?></title>


  <link href="<?= base_url();?>public/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?= base_url()?>public/js/html5shiv.js"></script>
  <script src="<?= base_url()?>public/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="signin">


<section>
  
  <div class="signinpanel">
    
    <div class="row">
      <div class="col-md-offset-3 col-md-5">
        <form method="post" action="<?=base_url()?>/user/loginproses">
          <img src="<?= base_url()?>/public/images/logoumm.jpg" style="width: 100%">
          <input type="text" class="form-control uname" placeholder="Username" name="username"/>
          <input type="password" class="form-control pword" placeholder="Password" name="password" />
          <!-- <a href=""><small>Forgot Your Password?</small></a> -->
          <button class="btn btn-block" type="submit">LOGIN</button>
          
        </form>
      </div><!-- col-sm-5 -->
      
    </div><!-- row -->
    
    <!-- <div class="signup-footer">
      <div class="pull-left">
        &copy; 2014. All Rights Reserved. Bracket Bootstrap 3 Admin Template
      </div>
      <div class="pull-right">
        Created By: <a href="http://themepixels.com/" target="_blank">ThemePixels</a>
      </div>
    </div> -->
    
  </div><!-- signin -->
  
</section>


<script src="<?= base_url()?>public/js/jquery-1.11.1.min.js"></script>
<script src="<?= base_url()?>public/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>public/js/modernizr.min.js"></script>
<script src="<?= base_url()?>public/js/jquery.sparkline.min.js"></script>
<script src="<?= base_url()?>public/js/jquery.cookies.js"></script>

<script src="<?= base_url()?>public/js/toggles.min.js"></script>
<script src="<?= base_url()?>public/js/retina.min.js"></script>

<script src="<?= base_url()?>public/js/custom.js"></script>
<script>
  jQuery(document).ready(function(){
    
    // Please do not use the code below
    // This is for demo purposes only
    var c = jQuery.cookie('change-skin');
    if (c && c == 'greyjoy') {
      jQuery('.btn-success').addClass('btn-orange').removeClass('btn-success');
    } else if(c && c == 'dodgerblue') {
      jQuery('.btn-success').addClass('btn-primary').removeClass('btn-success');
    } else if (c && c == 'katniss') {
      jQuery('.btn-success').addClass('btn-primary').removeClass('btn-success');
    }
  });
</script>

</body>
</html>