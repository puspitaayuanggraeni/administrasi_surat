<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/ico" href="<?= base_url()?>public/images/favicon.ico" />


  <title><?= title_app;?></title>

  <script src="<?= base_url()?>public/js/jquery-1.11.1.min.js"></script>
  <link href="<?= base_url()?>public/css/style.default.css" rel="stylesheet">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?= base_url()?>public/js/html5shiv.js"></script>
  <script src="<?= base_url()?>public/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="horizontal-menu">


<section>
  
  <div class="mainpanel">
    
    <div class="headerbar">
      
      <div class="header-left">
        
        <div class="logopanel">
        <!-- <h1><span>[</span> BO UMM <span>]</span></h1> -->
        <a href="<?= base_url().'user/dasbor'?>">
          <img src="<?= base_url()?>public/images/logoumm.jpg" style="width: 100%">
        </a>
        </div><!-- logopanel -->
        
        <div class="topnav">
            <a class="menutoggle"><i class="fa fa-bars"></i></a>
            
            <ul class="nav nav-horizontal">
                <li><a href="<?= base_url()?>user/index"><i class="fa fa-users"></i> <span>User</span></a></li>
                <li><a href="<?= base_url()?>jenis/index"> <i class="fa fa-list"></i>Jenis Surat</a></li>
                <li><a href="<?= base_url()?>surat/masuk"> <i class="glyphicon glyphicon-import"></i>Surat Masuk</a></li>
                <li><a href="<?= base_url()?>surat/keluar"> <i class="glyphicon glyphicon-export"></i>Surat Keluar</a></li>
            </ul>
        </div><!-- topnav -->
          
      </div><!-- header-left -->
      
      <div class="header-right">
        <ul class="headermenu">
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user"></i>
                <?= $user->username;?>(<?= $user->jabatan;?>)
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                <li><a href="<?= base_url()?>/user/logout"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div><!-- header-right -->
      
    </div><!-- headerbar -->
        
    <div class="pageheader">
      <h2><i class="fa fa-university"></i> <?= $title;?></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><label class="label label-primary" style="color:#ffffff"><?= date("d M Y - H:i");?></label></li>
          <!-- <li class="active">Horizontal Menu</li> -->
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
      <!-- content goes here... -->

