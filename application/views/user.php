<link href="<?= base_url()?>public/css/bootstrap-editable.css" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url()?>public/css/jquery.datatables.css">
<div class="row">
  <div class="col-md-offset-2 col-sm-8">
    <?php
      $hidden = array('id_user'=>'');
      echo form_open('user/insert', ['class'=>'form-horizontal'], $hidden);
    ?>
      <div class="panel panel-danger">
      <div class="panel-heading">
        <div class="panel-btns">
          <a href="" class="minimize">&minus;</a>
        </div>
        <h4 class="panel-title"><i class="fa fa-plus-circle"></i> Tambah Baru</h4>
      </div>
      <div class="panel-body">
        <div class="row">
        <div class="col-sm-12">
          <div class="form-group">
            <?= form_label('Username', 'username', ['class'=>'control-label col-md-3']);?>
          <div class="col-sm-8">
            <?= form_input(['name'=>'username', 'class'=>'form-control input-sm', 'value'=> set_value('username') ]);?>
            <?php echo form_error('username'); ?>
          </div>
          </div>

          <div class="form-group">
            <?= form_label('Password', 'password', ['class'=>'control-label col-md-3']);?>
          <div class="col-sm-8">
            <?= form_input(['name'=>'password', 'class'=>'form-control input-sm', 'value'=> set_value('password') ]);?>
            <?php echo form_error('password'); ?>
          </div>
          </div>

        
        
          <div class="form-group">
          <?= form_label('Jabatan', 'jabatan', ['class'=>'control-label col-md-3']);?>
              
          <div class="col-sm-8">
          <?php  $jabatan = [
              'Ka. Biro' => 'Ka. Biro',
              'Pembantu Rektor' => 'Pembantu Rektor',
              'Admin' => 'Admin',
              'Staff' => 'Staff'
          ];
          echo form_dropdown('jabatan', $jabatan,'', 'class="form-control input-sm"');?>
          <?php echo form_error('jabatan'); ?>
          </div>
          </div>
        
          <div class="form-group">
          <?= form_label('Hak Akses', 'bagian', ['class'=>'control-label col-md-3', 'value'=> set_value('username') ]);?>
          <div class="col-sm-8">
          <?php 
            $options = [
              "akses1"=>"akses1",
              "akses2"=>"akses2",
              "akses3"=>"akses3"
            ];
            echo form_dropdown('bagian', $options,'', 'class="form-control input-sm"');
            echo form_error('bagian');
          ?>
          </div>
          </div>
        
        </div>
      </div>        
      </div><!-- panel-body -->
      <div class="panel-footer">
      <div class="pull-right">
        <?= form_submit('submit', 'SIMPAN', ['class'=> 'btn btn-xs btn-primary']);?>
      </div>
      </div>
      </div>
    <?php form_close(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-danger">
      <div class="panel-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>USERNAME</th>
              <th>JABATAN</th>
              <th>HAK AKSES</th>
              <th>AKSI</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($users as $data): ?>
              <tr>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id_user;?>" field="username">
                    <?= $data->username;?>
                  </a>
                </td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id_user;?>" field="jabatan">
                    <?= $data->jabatan;?>
                  </a>
                </td>
                <td>
                  <a class="_bagian" data-type="select" pk="<?=$data->id_user;?>" field="bagian" data-value="<?= $data->bagian;?>">
                    <?= $data->bagian;?>
                  </a>
                </td>
                <td>
                  <div class="btn-group">
                  <a class="btn btn-danger btn-xs" onClick="return confirm('Anda yakin akan meghapus data ?');" title="Hapus" href="<?= base_url()?>user/delete?id=<?=$data->id_user;?>"> <i class="fa fa-trash-o"></i></a>  
                  </div>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
  </div>
</div>
<script src="<?= base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap-editable.min.js"></script> 
<script src="<?= base_url()?>public/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>public/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>public/js/select2.min.js"></script> 
<script type="text/javascript">
  var base_url = "<?= base_url();?>";
  $(".table").DataTable({
    "fnPreDrawCallback": function (oSettings) {
      editAble();
    }
  });  
  $('select[name=DataTables_Table_0_length]').select2();
  function editAble(){
  $('._input_user').editable({
      mode: 'inline',
      success: function(response, newValue) {
        var id = $(this).attr('pk');
        var field = $(this).attr('field');
        $.ajax({
          type: "POST",
          url: base_url+'user/update', 
          data: { id: id, value: newValue, field: field },
          success: function(data) {
            if(data!="success")
              alert(data);
          }
        });
      }
  });

    $('._bagian').editable({
        source: [
                 {value: "akses1", text: "akses1"},
                 {value: "akses2", text: "akses2"},
                 {value: "akses3", text: "akses3"}
                ],
        success: function(response, newValue) {
          var id = $(this).attr('pk');
          var field = $(this).attr('field');
          $.ajax({
            type: "POST",
            url: base_url + 'user/update', 
            data: { id: id, value: newValue, field: field },
            success: function(data) {
              if(data!="success")
                alert(data);
            }
          });
        }
    });  
  }
</script>