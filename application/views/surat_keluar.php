<link href="<?= base_url()?>public/css/bootstrap-editable.css" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url()?>public/css/jquery.datatables.css">

<style type="text/css">
  tfoot{
    display: table-header-group;
  }
  tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
   }  
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding: 2px !important;
  }
  .dataTables_info { 
    display: none; 
  }

  .dataTables_filter{
    display: none;
  }
</style>
<div class="row">
    <div class="col-md-offset-1 col-sm-10">
    <form action='<?=base_url()?>surat_keluar/insert' class="form-horizontal" method="POST" >
      <div class="panel panel-danger">
      <div class="panel-heading">
        <div class="panel-btns">
          <a href="" class="minimize">&minus;</a>
        </div>
        <h4 class="panel-title"><i class="fa fa-plus-circle"></i> Tambah Baru</h4>
      </div>
      <div class="panel-body">
      <?php include("_formSK.php"); ?>
      <div class="panel-footer mt10">
      <div class="pull-right">
        <?= form_submit('submit', 'SIMPAN', ['class'=> 'btn btn-xs btn-primary']);?>
      </div>
      </div>
      </div>
    </form>
  </div>
</div>

  <div class="col-md-10 col-md-offset-1 anggra">
    <form class="form-horizontal" action="<?= base_url()?>mpdf/examples/printLaporanSuratKeluar.php" method="POST">
      <div class="panel panel-danger">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-print"></i> CETAK LAPORAN SURAT</h3>
        </div>
        <div class="panel-body">
              
              <div class="col-sm-4">
              <div class="form-group">
                <label class="col-sm-3 control-label">Dari</label>
                <div class="col-sm-6">
                  <?= form_input(['name'=>'dari', 'class'=>'form-control datepicker', 'value'=>  date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d')))) ]);?>
                  <?php echo form_error('dari'); ?>
                </div>
              </div>
              </div>

              <div class="col-sm-4">
              <div class="form-group">
                <label class="col-sm-3 control-label">Sampai</label>
                <div class="col-sm-6">
                  <?= form_input(['name'=>'sampai', 'class'=>'form-control datepicker', 'value'=>  date('Y-m-d')]);?>
                  <?php echo form_error('sampai'); ?>
                </div>
              </div>
              </div>
              <div class="col-sm-3">
                <button class="btn btn-xs pull-left btn-primary"><i class="fa fa-print"></i> Cetak</button>
              </div>

        </div>
      </div>
    </form>
  </div>

<div class="row">
  <div class="col-md-12">
      <div class="panel panel-danger">
      <div class="panel-body">
        <table class="table table-striped" id="daftar">
          <thead>
            <tr>
              <th>NOMOR</th>
              <th>Dri NAMA</th>
              <th>Dri JABATAN</th>
              <th>PERIHAL</th>
              <th>KEPADA</th>
              <th>TGL SURAT</th>
              <th>AKSI</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            </tr>
          </tfoot>
          <tbody>
            <?php foreach ($model as $data): ?>
              <tr>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="Nomor">
                    <?= $data->nomor.$data->id;?>
                  </a>
                </td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="Dari_nama">
                    <?= $data->dari_nama;?>
                  </a>
                </td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="Dari_jabatan">
                    <?= $data->dari_jabatan;?>
                  </a>
                </td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="Perihal">
                    <?= $data->perihal;?>
                  </a>
                </td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="Kepada">
                    <?= $data->kepada;?>
                  </a>
                </td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="Tgl_Surat">
                    <?= $data->tgl_surat;?>
                  </a>
                </td>
                <td>
                  <div class="btn-group">
                  <a class="btn btn-danger btn-xs" title="Hapus" href="<?= base_url()?>surat_masuk/delete?id=<?=$data->id;?>"> <i class="fa fa-trash-o"></i></a>
                  <a target="_blank" href="<?= base_url().'mpdf/examples/print.php?id='.$data->id;?>" class="btn btn-primary btn-xs" title="Lihat Surat"> <i class="fa fa-file-photo-o"></i></a>
                  <a href="<?= base_url().'surat/keluar/edit?id='.$data->id;?>" class="btn btn-success btn-xs" title="Edit Surat"> <i class="fa fa-edit"></i></a>
                  </div>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
  </div>
</div>

<script type="text/javascript">
  var base_url = "<?= base_url();?>";
  var no = 0;
  $('#daftar tfoot th').each( function () {
    no=no+1;
    var title = $(this).text();
      $(this).html( '<input type="text" class="form-control input-sm src'+(no)+'" placeholder="'+title+'" />' );
  });

  
  var t = $("#daftar").DataTable({
    "fnPreDrawCallback": function (oSettings) {
      editAble();
    }
  });  
  t.columns().every(function(){
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    });

  $('select[name=daftar_length]').select2();
  $('.src7').hide();
    $('.datepicker').datepicker({
    "format": "yyyy-mm-dd"
  });
function editAble(){
  $('._input_user').editable({
      mode: 'inline',
      success: function(response, newValue) {
        var id = $(this).attr('pk');
        var field = $(this).attr('field');
        $.ajax({
          type: "POST",
          url: base_url+'surat_keluar/update', 
          data: { id: id, value: newValue, field: field },
          success: function(data) {
            if(data!="success")
              alert(data);
          }
        });
      }
  });

    $('._jenis').editable({
        source: <?php echo json_encode($listjenis);?>,
        success: function(response, newValue) {
          var id = $(this).attr('pk');
          var field = $(this).attr('field');
          $.ajax({
            type: "POST",
            url: base_url + 'surat_keluar/update', 
            data: { id: id, value: newValue, field: field },
            success: function(data) {
              if(data!="success")
                alert(data);
            }
          });
        }
    });  
    }
</script>