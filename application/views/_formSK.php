<link rel="stylesheet" href="<?= base_url()?>public/css/bootstrap-wysihtml5.css" />
<link rel="stylesheet" href="<?= base_url()?>public/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="<?= base_url()?>public/css/jquery.tagsinput.css" />

<div class="row">
  <div class="col-sm-12">

    <div class="form-group">
      <?= form_label('Jenis Surat', 'id_jenis', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
        <?php 
          echo "<select class='form-control' name='id_jenis'>";
          $listjenis= [];
          foreach ($jenis_surat as $data) {
            $selected = (getValue($values, 'id_jenis') == $data->id) ? 'selected' : (getValue($values, 'kode_surat'));
            echo "<option $selected value='$data->id' kode_surat='$data->kode_surat'> $data->jenis_surat</option>";
            $listjenis[] = ['value'=> "$data->id", 'text'=> "$data->jenis_surat"];
          }
          echo"<select>";
          echo form_error('id_jenis');
        ?>
      </div>
    </div>

    <div class="form-group">
      <?= form_label('Dari', 'Dari', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-5"> 
      <?= form_input(['name'=>'Dari_jabatan', 'class'=>'form-control input-sm', 'value'=> set_value('Dari_jabatan', getValue($values, 'dari_jabatan')),'placeholder'=>'Bagian']);?>
      <?php echo form_error('Dari_jabatan'); ?>
      </div>
      <div class="col-sm-5"> 
      <?= form_input(['name'=>'Dari_nama', 'class'=>'form-control input-sm', 'value'=> set_value('Dari_nama', getValue($values, 'dari_nama') ),'placeholder'=>'Nama' ]);?>
      <?php echo form_error('Dari_nama'); ?>
      </div>
    </div>
    <div class="form-group inline">
      <?= form_label('Nomor Surat', 'Nomor', ['class'=>'control-label col-sm-2']);?>
    <?php if(isset($_GET['id']) != TRUE) : ?>
      <div class="col-sm-2"><?= form_input(['name'=>'kode_surat', 'class'=>'form-control input-sm', 'readonly'=>'readonly', 'placeholder'=>'Kode Surat']);?>
      </div>
      <div class="col-sm-1"><?= form_input(['class'=>'form-control input-sm', 'readonly'=>'readonly', 'value'=>'/']);?></div>
      <div class="col-sm-2"><?= form_input(['name'=>'Nomor', 'class'=>'form-control input-sm', 'value'=> set_value('Nomor'), 'placeholder'=>'No Surat']);?></div>
      <?php
      $k = '/UMM/';
      if($user->jabatan == 'Ka. Biro')
          $k = '/MAWA-UMM/';
      ?>
      <div class="col-sm-5"> <?= form_input(['name'=>'Nomor2' ,'class'=>'form-control input-sm', 'readonly'=>'readonly', 'value'=>$k.getRomawi(date('m')).'/'.date('Y')]);?></div>
    <?php else: ?>
      <div class="col-sm-10"><?= form_input(['name'=>'Nomor', 'class'=>'form-control input-sm', 'value'=> set_value('Nomor'), 'placeholder'=>'No Surat', 'value'=> set_value('Nomor', getValue($values, 'nomor'))]);?></div>
    <?php endif;?>
      <?= form_label('','', ['class'=>'control-label col-sm-2']);?>
      <?php echo form_error('Nomor'); ?>

    </div>

    <div class="form-group">
      <?= form_label('Lampiran', 'Lampiran', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
      <?= form_input(['name'=>'Lampiran', 'class'=>'form-control input-sm', 'value'=> set_value('Lampiran',getValue($values, 'lampiran')) ]);?>
      <?php echo form_error('Lampiran'); ?>
      </div>
    </div>

    <div class="form-group">
      <?= form_label('Perihal', 'Perihal', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
      <?= form_input(['name'=>'Perihal', 'class'=>'form-control input-sm', 'value'=> set_value('Perihal', getValue($values, 'perihal'))]);?>
      <?php echo form_error('Perihal'); ?>
      </div>
    </div>

    <div class="form-group">
      <?= form_label('Kepada', 'Kepada', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
      <?= form_input(['name'=>'Kepada', 'class'=>'form-control input-sm tags', 'value'=> set_value('Kepada', getValue($values, 'kepada'))]);?>
      <?php echo form_error('Kepada'); ?>
      </div>
    </div>

    <div class="form-group">
      <?= form_label('Tembusan', 'Tembusan', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
      <?= form_input(['name'=>'Tembusan', 'class'=>'form-control input-sm tags', 'value'=> set_value('Tembusan', getValue($values, 'tembusan'))]);?>
      <?php echo form_error('Tembusan'); ?>
      </div>
    </div>

    <div class="form-group">
      <?= form_label('Atas Nama', 'atas_nama', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
      <?= form_input(['name'=>'atas_nama', 'class'=>'form-control input-sm ', 'value'=> set_value('atas_nama', getValue($values, 'atas_nama'))]);?>
      <?php echo form_error('atas_nama'); ?>
      </div>
    </div>

    <div class="form-group">
      <?= form_label('Lokasi Tujuan Surat', 'lokasi_tujuan', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
      <?= form_input(['name'=>'lokasi_tujuan', 'class'=>'form-control input-sm ', 'value'=> set_value('lokasi_tujuan', getValue($values, 'lokasi_tujuan'))]);?>
      <?php echo form_error('lokasi_tujuan'); ?>
      </div>
    </div>

    <div class="form-group">
      <?= form_label('Tgl Surat', 'Tgl_surat', ['class'=>'control-label col-sm-2']);?>
      <div class="col-sm-10">
      <?= form_input(['name'=>'Tgl_surat', 'class'=>'form-control datepicker', 'value'=> set_value('Tgl_surat', getValue($values, 'tgl_surat'))]);?>
      <?php echo form_error('Tgl_surat'); ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-12">
      <?= form_label('Isi Surat', 'Isi', ['class'=>'control-label col-sm-2']);?>
      <?= form_textarea(['name'=>'Isi', 'id'=>'Isi', 'class'=>'form-control input-sm','type'=>'textarea','rows'=>10, 'placeholder'=>'Isi Surat', 'value'=> set_value('Isi', getValue($values, 'isi'))]);?>
      <?php echo form_error('Isi'); ?>
      </div>
    </div>

  </div>
</div>
<script src="<?= base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap-editable.min.js"></script> 
<script src="<?= base_url()?>public/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>public/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>public/js/select2.min.js"></script> 
<script src="<?= base_url()?>public/js/bootstrap-timepicker.min.js"></script>
<script src="<?= base_url()?>public/js/jquery.tagsinput.min.js"></script>
<script src="<?= base_url()?>public/js/wysihtml5-0.3.0.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript">
  $('#Isi').wysihtml5({color: true,html:true});
  $('.datepicker, #daftar_wrapper #daftar_filter .datepicker').datepicker({
    "format": "yyyy-mm-dd"
  });
  $('.tags').tagsInput({width:'auto', height:'auto', defaultText:'+ tambah'});
  
  $('select[name=id_jenis]').change(function(e){
    e.preventDefault();
    var selected =$('select[name=id_jenis]').find(":selected");
    $('input[name=kode_surat]').val(selected.attr('kode_surat'));
    // $('input[name=Perihal]').val(selected.text());
  });


</script>