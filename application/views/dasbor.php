<link href="<?= base_url()?>public/css/bootstrap-editable.css" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url()?>public/css/bootstrap-timepicker.min.css">
  

<div class="row">  
  <div class="col-md-6 col-md-offset-3 mt20">
    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title"><center> <?= strtoupper($user->username);?> </center></h4>
      </div>
      <div class="panel-body">
      <div class="col-md-4">
        <center><img src=<?= base_url()."public/images/photos/loggeduser.png"?> width="50%"></center>
      </div>
      <div class="col-md-8">
        <div class="col-md-12">
        <table class="table">
          <tr>
            <td>Jabatan</td>
            <td> : </td>
            <td><?= $user->jabatan;?></td>
          </tr>
          <tr>
            <td>Hak Akses</td>
            <td> : </td>
            <td><?= $user->bagian;?></td>
          </tr>
        </table>
        </div>
      </div>
      </div>
      </div>
    </div>
  </div>
<!-- 
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-body">
        <b><i class="glyphicon glyphicon-import"></i> JUMLAH SURAT MASUK</b>
        <div class="mb10"></div>
        <div class="col-md-4">
          <div class="panel panel-warning panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">HARI </small>
                    <h1><?= $sMasuk['hari'];?></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">BULAN</small>
                    <h1><?= $sMasuk['bulan'];?></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-primary panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">TAHUN</small>
                    <h1><?= $sMasuk['tahun'];?></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <b><i class="glyphicon glyphicon-export"></i> JUMLAH SURAT KELUAR</b>
        <div class="mb10"></div>
        <div class="col-md-4">
          <div class="panel panel-warning panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">HARI </small>
                    <h1><?= $sKeluar['hari'];?></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">BULAN</small>
                    <h1><?= $sKeluar['bulan'];?></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-primary panel-stat">
            <div class="panel-heading">
              <div class="stat">
                <div class="row">
                  <div class="col-xs-12">
                    <small class="stat-label">TAHUN</small>
                    <h1><?= $sKeluar['tahun'];?></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <form class="form-horizontal form-bordered" action="<?= base_url()?>mpdf/examples/printLaporanSurat.php" method="POST">
      <div class="panel panel-danger">
        <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-print"></i> CETAK LAPORAN SURAT</h3>
        </div>
        <div class="panel-body">
              
              <div class="form-group">
                <label class="col-sm-3 control-label">Dari Tanggal</label>
                <div class="col-sm-6">
                  <?= form_input(['name'=>'dari', 'class'=>'form-control datepicker', 'value'=>  date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d')))) ]);?>
                  <?php echo form_error('dari'); ?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Sampai Tanggal</label>
                <div class="col-sm-6">
                  <?= form_input(['name'=>'sampai', 'class'=>'form-control datepicker', 'value'=>  date('Y-m-d')]);?>
                  <?php echo form_error('sampai'); ?>
                </div>
              </div>

        </div>
        <div class="panel-footer">
          <button class="btn btn-xs pull-right btn-primary"><i class="fa fa-print"></i> Cetak</button>
        </div>
      </div>
    </form> -->
</div>

<script src="<?= base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap-timepicker.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap-editable.min.js"></script> 
<script type="text/javascript">
  var base_url = "<?= base_url();?>";
  $('.datepicker').datepicker({
    "format": "yyyy-mm-dd"
  });
  
</script>