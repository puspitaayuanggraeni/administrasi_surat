<link rel="stylesheet" href="<?= base_url() ?>public/css/bootstrap-timepicker.min.css">
<link href="<?= base_url() ?>public/css/bootstrap-editable.css" rel="stylesheet">
<div class="col-sm-12">
  <div class="form-group">
    <?= form_label('Dari', 'Dari', ['class' => 'control-label col-sm-3']); ?>
    <div class="col-sm-8">
      <?= form_input(['name' => 'Dari', 'class' => 'form-control input-sm', 'value' => set_value('Dari', getValue($values, 'dari'))]); ?>
      <?php echo form_error('Dari'); ?>
    </div>
  </div>

  <div class="form-group">
    <?= form_label('Nomor', 'Nomor', ['class' => 'control-label col-sm-3']); ?>
    <div class="col-sm-8">
      <?= form_input(['name' => 'Nomor', 'class' => 'form-control input-sm', 'value' => set_value('Nomor', getValue($values, 'nomor'))]); ?>
      <?php echo form_error('Nomor'); ?>
    </div>
  </div>

  <div class="form-group">
    <?= form_label('Lampiran', 'Lampiran', ['class' => 'control-label col-sm-3']); ?>
    <div class="col-sm-8">
      <?= form_input(['name' => 'Lampiran', 'class' => 'form-control input-sm', 'value' => set_value('Lampiran', getValue($values, 'lampiran'))]); ?>
      <?php echo form_error('Lampiran'); ?>
    </div>
  </div>

  <div class="form-group">
    <?= form_label('Perihal', 'Perihal', ['class' => 'control-label col-sm-3']); ?>
    <div class="col-sm-8">
      <?= form_input(['name' => 'Perihal', 'class' => 'form-control input-sm', 'value' => set_value('Perihal', getValue($values, 'perihal'))]); ?>
      <?php echo form_error('Perihal'); ?>
    </div>
  </div>
  <div class="form-group">
    <?= form_label('Kepada', 'Kepada', ['class' => 'control-label col-sm-3']); ?>
    <div class="col-sm-8">
      <?= form_input(['name' => 'Kepada', 'class' => 'form-control input-sm', 'value' => set_value('Kepada', getValue($values, 'kepada'))]); ?>
      <?php echo form_error('Kepada'); ?>
    </div>
  </div>
  <div class="form-group">
    <?= form_label('Tanggal Surat', 'Tgl_Surat', ['class' => 'control-label col-sm-3']); ?>
    <div class="col-sm-8">
      <?= form_input(['name' => 'Tgl_Surat', 'class' => 'form-control input-sm datepicker', 'value' => set_value('Tgl_Surat', getValue($values, 'tgl_surat')), 'placeholder' => "yyyy/mm/dd"]); ?>
      <?php echo form_error('Tgl_Surat'); ?>
    </div>
  </div>
  <div class="form-group">
    <?= form_label('Isi', 'Isi', ['class' => 'control-label col-sm-3']); ?>
    <div class="col-sm-8">
      <?= form_upload(['name' => 'Isi[]', 'class' => 'form-control input-sm', 'multiple' => true]); ?>
      <?php echo '<label class="error">' . implode(", ", $error) . '</label>'; ?>
    </div>
  </div>
</div>
<?php if ($isNewRecord == FALSE) { ?>
  <div class="col-md-12 mt10 col-md-offset-3" >
  <?php foreach ($scan as $key => $value) { ?>
      <div class="col-sm-4 col-md-2">
        <div class="panel panel-default widget-photoday">
          <div class="panel-body">
            <a href="" class="photoday"><img  class='custom' src="<?= base_url() . 'uploads/' . $value->image; ?>" alt=""></a>
            <ul class="photo-meta">
              <li><a href="<?= base_url() . 'uploads/' . $value->image; ?>" target="_blank"><i class="fa fa-search"></i> Zoom</a></li>
              <li><a href='<?= base_url() . "surat_masuk/deletescan?image=$value->image&id=$_GET[id]"; ?>'><i class="fa fa-trash-o"></i> Hapus</a></li>
            </ul>
          </div><!-- panel-body -->
        </div><!-- panel -->
      </div>
  <?php } ?>
  </div>
<?php } ?>  
<div class="row">
  <div class="col-sm-12">  
    <div class="row"><hr>
      <h5 class="subtitle mt10 mb10"> <i class="fa fa-refresh"></i> DISPOSISI
      </h5> 
    </div>
    <div class="col-sm-4">
      <div class="form-group">
          <?= form_label('Disposisi', 'Kepada2', ['class' => 'control-label col-sm-3']); ?>
        <div class="col-sm-8">
          <?= form_input(['name' => 'Kepada2', 'class' => 'form-control input-sm', 'value' => set_value('Kepada2', getValue($values, 'disposisi_kepada'))]); ?>
<?php echo form_error('Kepada2'); ?>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
          <?= form_label('Tanggal', 'tgl_disposisi', ['class' => 'control-label col-sm-3']); ?>
        <div class="col-sm-8">
          <?= form_input(['name' => 'tgl_disposisi', 'class' => 'form-control input-sm datepicker', 'value' => set_value('tgl_disposisi', getValue($values, 'tgl_disposisi')), 'placeholder' => "yyyy/mm/dd"]); ?>
<?php form_error('tgl_disposisi') ?>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
          <?= form_label('Keterangan', 'Keterangan', ['class' => 'control-label col-sm-3']); ?>
        <div class="col-sm-8">
          <?= form_input(['name' => 'Keterangan', 'class' => 'form-control input-sm', 'value' => set_value('Keterangan', getValue($values, 'keterangan'))]); ?>
<?= form_error('Keterangan'); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?= base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap-editable.min.js"></script> 
<script src="<?= base_url()?>public/js/select2.min.js"></script> 
<script src="<?= base_url()?>public/js/bootstrap-timepicker.min.js"></script>
  <script type="text/javascript">
    $('.datepicker').datepicker({
    "format": "yyyy-mm-dd"
  });

  </script>  