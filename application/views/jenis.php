<link href="<?= base_url()?>public/css/bootstrap-editable.css" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url()?>public/css/jquery.datatables.css">
<div class="row">
  <div class="col-md-offset-1 col-sm-10">
    <?php
      $hidden = array('id'=>'');
      echo form_open('jenis/insert', ['class'=>'form-horizontal'], $hidden);
    ?>
      <div class="panel panel-danger">
      <div class="panel-heading">
        <h4 class="panel-title"><i class="fa fa-plus-circle"></i> Tambah Baru</h4>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="form-group">
            <?= form_label('Kode', 'kode_surat', ['class'=>'control-label col-sm-3']);?>
            <div class="col-sm-6">
              <?= form_input(['name'=>'kode_surat', 'class'=>'form-control input-sm', 'value'=> set_value('kode_surat') ]);?>
              <?php echo form_error('kode_surat'); ?>
            </div>
          </div>
          <div class="form-group">
            <?= form_label('Jenis', 'jenis_surat', ['class'=>'control-label col-sm-3']);?>
            <div class="col-sm-6">
              <?= form_input(['name'=>'jenis_surat', 'class'=>'form-control input-sm', 'value'=> set_value('jenis_surat') ]);?>
              <?php echo form_error('jenis_surat'); ?>
            </div>
          </div>
        </div><!-- row -->
      </div><!-- panel-body -->
      <div class="panel-footer">
      <div class="pull-right">
        <?= form_submit('submit', 'SIMPAN', ['class'=> 'btn btn-xs btn-primary']);?>
      </div>
      </div>
      </div>
    <?php form_close(); ?>
  </div>
<!-- </div>
<div class="row"> -->
  <div class="col-sm-12">
      <div class="panel panel-danger">
      <div class="panel-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>NOMOR</th>
              <th>KODE</th>
              <th>JENIS</th>
              <th>AKSI</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $no = 0;
              foreach ($jenis as $data): 
              $no++;
            ?>
              <tr>
                <td><?= $no;?></td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="kode_surat">
                    <?= $data->kode_surat;?>
                  </a>
                </td>
                <td>
                  <a class="_input_user" data-type="text" pk="<?=$data->id;?>" field="jenis_surat">
                    <?= $data->jenis_surat;?>
                  </a>
                </td>
                <td>
                  <div class="btn-group">
                  <a class="btn btn-danger btn-xs" title="Hapus" href="<?= base_url()?>jenis/delete?id=<?=$data->id;?>"> <i class="fa fa-trash-o"></i></a>  
                  </div>
                </td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
  </div>
</div>
<script src="<?= base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>public/js/bootstrap-editable.min.js"></script> 
<script src="<?= base_url()?>public/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>public/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>public/js/select2.min.js"></script> 
<script type="text/javascript">
  var base_url = "<?= base_url();?>";
  $(".table").DataTable({
     "fnPreDrawCallback": function( oSettings ) {
       editAble();
     }
  });  
  $('select[name=DataTables_Table_0_length]').select2();
  function editAble(){
    $('._input_user').editable({
        mode: 'inline',
        success: function(response, newValue) {
          var id = $(this).attr('pk');
          var field = $(this).attr('field');
          $.ajax({
            type: "POST",
            url: base_url+'jenis/update', 
            data: { id: id, value: newValue, field: field },
            success: function(data) {
              if(data!="success")
                alert(data);
            }
          });
        }
    });
  }

</script>