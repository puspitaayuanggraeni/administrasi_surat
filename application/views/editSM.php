<div class="row">
  <div class="col-md-offset-1 col-sm-10">
    <?php
    $hidden = array('id_user' => '');
    echo form_open(base_url() . 'surat/masuk/updateall?id=' . $_GET['id'], ['id' => 'form', "class" => "form-horizontal","enctype"=>"multipart/form-data"], $hidden);
    ?>
    <div class="panel panel-danger">
      <div class="panel-heading">
        <h4 class="panel-title"><i class="fa fa-edit"></i> <?= getValue($values, 'nomor'); ?></h4>
      </div>
      <div class="panel-body">
        <?php include("_formSM.php"); ?>
      </div>
      <div class="panel-footer mt10">
        <div class="pull-right">
          <?= form_submit('submit', 'SIMPAN', ['class' => 'btn btn-xs btn-primary']); ?>
        </div>
      </div>
    </div>
    <?php form_close(); ?>
  </div>
</div>