<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('ACTIVATE_EMAIL', FALSE);
define('ACTIVATE_INDEX_PHP', TRUE);

function load_view($obj, $view, $data = array(), $session_not_null = FALSE)  {
  if(!isset($_SESSION["id_user"])) {
   redirect(base_url()+'/user/login', 'refresh');
  }
  $id_user = $_SESSION["id_user"];
  $data['user'] = $obj->user->find(['id_user' => $id_user])[0];
  $obj->load->view('templates/header', $data);
  $obj->load->view($view, $data);
  $obj->load->view('templates/footer');
}

function getRomawi($angka){
  $romawi = [
    '01' =>'I',
    '02' => 'II',
    '03' => 'III',
    '04' => 'IV',
    '05' =>'V',
    '06' => 'VI',
    '07' => 'VII',
    '08' => 'VIII',
    '09' => 'IX',
    '10'  =>'X',
    '11'  => 'XI',
    '12'  => 'XII'
  ];

  return $romawi[$angka];
}

function hakAkses($obj, $halaman){
    $daftarAkses = [
      'user1'=>['dasbor', 'jSurat','sMasuk', 'user', 'sKeluar'],
      'user2'=>['dasbor', 'sMasuk'],
      'user3'=>['dasbor', 'sKeluar'] 
    ];
  $id_user = $_SESSION["id_user"];
  $bagian = $obj->user->find(['id_user' => $id_user])[0];
  return(in_array($halaman,$daftarAkses[$bagian->bagian]));
}

function getValue($array=[], $key){
    return isset($array[$key]) ? $array[$key] : '';
}